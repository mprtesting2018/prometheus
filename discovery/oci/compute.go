package oci

import (
	"context"

	"fmt"
	"net"

	"time"

	"github.com/go-kit/kit/log/level"
	"github.com/go-kit/log"
	"github.com/oracle/oci-go-sdk/common"
	"github.com/oracle/oci-go-sdk/common/auth"
	"github.com/oracle/oci-go-sdk/core"
	"github.com/oracle/oci-go-sdk/identity"
	"github.com/pkg/errors"
	"github.com/prometheus/common/config"

	"github.com/prometheus/common/model"
	"github.com/prometheus/prometheus/discovery"
	"github.com/prometheus/prometheus/discovery/refresh"
	"github.com/prometheus/prometheus/discovery/targetgroup"
)

const (
	ociLabel                    = model.MetaLabelPrefix + "oci_"
	ociLabelRegion              = ociLabel + "region"
	ociLabelAvailabilityDomain  = ociLabel + "availability_domain"
	ociLabelFaultDomain         = ociLabel + "fault_domain"
	ociLabelFullCompartmentName = ociLabel + "compartment"
	ociLabelCompartmentID       = ociLabel + "compartment_id"
	ociLabelInstanceID          = ociLabel + "instance_id"
	ociLabelInstanceName        = ociLabel + "instance_name"
	ociLabelInstanceState       = ociLabel + "instance_state"
	ociLabelPrivateIP           = ociLabel + "private_ip"
	ociLabelPublicIP            = ociLabel + "public_ip"
	ociLabelDefinedTag          = ociLabel + "defined_tag_"
	ociLabelFreeformTag         = ociLabel + "freeform_tag_"
	ociLabelInstancePoolName    = ociLabel + "instance_pool"
	ociLabelOkeClusterName      = ociLabel + "oke_cluster"

	ociLabelShape                = ociLabel + "shape"
	ociLabelOcpus                = ociLabel + "ocpus"
	ociLabelProcessorDescription = ociLabel + "processor_description"

	ociLabelVcnId    = ociLabel + "vcn_id"
	ociLabelVcn      = ociLabel + "vcn"
	ociLabelSubnetId = ociLabel + "subnet_id"
)

// default SD configuration.
var DefaultSDConfig = OciSDConfig{
	Port:            80,
	RefreshInterval: model.Duration(5 * 60 * time.Second),

	profileName:        "DEFAULT",
	privateKeyPassword: "",
	authType:           "InstancePrincipal",
}

// OciSDConfig is the configuration for OCI based service discovery.
type OciSDConfig struct {
	Tenancy            string          `yaml:"tenancy_ocid"`
	Region             string          `yaml:"region"`
	Compartment        string          `yaml:"compartment_ocid,omitempty"`
	authType           string          `yaml:"auth_type,omitempty"`
	configFilePath     string          `yaml:"config_file_path,omitempty"`
	profileName        string          `yaml:"profile_name,omitempty"`
	privateKeyPassword config.Secret   `yaml:"pvt_key_password,omitempty"`
	TagFilters         []*FilterByTags `yaml:"tag_filters,omitempty"`
	Port               int             `yaml:"port"` // port from where to scrape metrics on targets
	RefreshInterval    model.Duration  `yaml:"refresh_interval,omitempty"`
}

// Filter is the configuration for filtering OCI Compute instances, when discovering targets.
type FilterByTags struct {
	TagNameSpace string `yaml:"tagNamespace,omitempty"`
	TagKey       string `yaml:"tagKey,omitempty"`
	TagValue     string `yaml:"tagValue,omitempty"`
}

// Name returns the name of the 'OCI SD Discovery' Config for Prometheus.
func (*OciSDConfig) Name() string { return "oci" }

func init() {
	discovery.RegisterConfig(&OciSDConfig{})
}

// UnmarshalYAML implements the yaml.Unmarshaler interface.
func (c *OciSDConfig) UnmarshalYAML(unmarshal func(interface{}) error) error {
	*c = DefaultSDConfig
	type plain OciSDConfig
	err := unmarshal((*plain)(c))
	if err != nil {
		return errors.Wrap(err, " Error in YML parsing OCI SD Configs")
	}

	if c.authType == "" {
		return errors.New("oci_authtype can not be empty")
	}

	if c.authType != "UserPrincipal" && c.authType != "InstancePrincipal" {
		return errors.New("Invalid value for oci_auth_type, only valid values are OciConfigFile or InstancePrincipal")
	}

	if c.Tenancy == "" {
		return errors.New("OCI SD configuration requires a Tenancy OCID")
	}
	if c.Region == "" {
		// TODO: optionally use instance metadata if Prometheus is running on OCI Compute Instance
		return errors.New("OCI SD configuration requires a Region")
	}

	if c.Compartment == "" {
		return errors.New("OCI SD configuration requires a Compartment OCID")
	}
	return nil
}

// Discovery periodically performs OCI-SD requests. It implements the Discoverer interface.
type Discovery struct {
	*refresh.Discovery
	cfg       *OciSDConfig
	ociConfig common.ConfigurationProvider //TODO
	logger    log.Logger
	port      int

	computeClient *core.ComputeClient
	vnwClient     *core.VirtualNetworkClient
	iamClient     *identity.IdentityClient
}

// NewDiscovery returns a new OCI Discovery which periodically refreshes its targets.
func NewDiscovery(cfg *OciSDConfig, logger log.Logger) (*Discovery, error) {
	if logger == nil {
		logger = log.NewNopLogger() // TODO : see logger
	}

	var ociConfig common.ConfigurationProvider
	var err error

	if cfg.authType == "UserPrincipal" {
		ociConfig, err = common.ConfigurationProviderFromFileWithProfile(cfg.configFilePath, cfg.profileName, string(cfg.privateKeyPassword))
		if err != nil {
			return nil, errors.Wrap(err, "error setting up OCI Auth clinet with OCI config file")
		}
	} else {
		ociConfig, err = auth.InstancePrincipalConfigurationProviderForRegion(common.StringToRegion(cfg.Region))
		if err != nil {
			return nil, errors.Wrap(err, "error setting up OCI Auth clinet with InstancePrincipal, make sure you are running Prometheus server on OCI Compute Node")
		}
	}

	d := &Discovery{
		cfg:       cfg,
		port:      cfg.Port,
		logger:    logger,
		ociConfig: ociConfig,
	}

	computeClient, err := core.NewComputeClientWithConfigurationProvider(d.ociConfig)
	if err != nil {
		return nil, errors.Wrap(err, "error setting up OCI Compute Client")
	}
	d.computeClient = &computeClient

	vnwClient, err := core.NewVirtualNetworkClientWithConfigurationProvider(d.ociConfig)
	if err != nil {
		return nil, errors.Wrap(err, "error setting up OCI Virtual Network Client")
	}
	d.vnwClient = &vnwClient

	identityClient, err := identity.NewIdentityClientWithConfigurationProvider(d.ociConfig)
	if err != nil {
		return nil, errors.Wrap(err, "error setting up OCI Identity Client")
	}
	d.iamClient = &identityClient

	d.Discovery = refresh.NewDiscovery(
		logger,
		"oci",
		time.Duration(cfg.RefreshInterval),
		d.refresh,
	)

	return d, nil
}

// NewDiscoverer returns a Discoverer for the Compute Config.
func (c *OciSDConfig) NewDiscoverer(opts discovery.DiscovererOptions) (discovery.Discoverer, error) {
	return NewDiscovery(c, opts.Logger)
}

func (d *Discovery) refresh(context context.Context) ([]*targetgroup.Group, error) {
	computeClient := d.computeClient

	tg := &targetgroup.Group{
		Source: d.cfg.Region,
	}

	tenancyCmptOcidToNameMap, err := d.getCompartments(context) // TODO: refresh only after 5 refresh cycles?
	if err != nil {
		return nil, fmt.Errorf("could not obtain list of compartments: %s", err)
	}

	mapSubnetIdToDetails, err := d.getVcnDetails(context, d.vnwClient, d.cfg.Compartment)
	if err != nil {
		mapSubnetIdToDetails = make(map[string]SubnetInfo)
	}

	instanceArr, err := computeClient.ListInstances(
		context,
		core.ListInstancesRequest{CompartmentId: &d.cfg.Compartment},
	)
	if err != nil {
		return nil, fmt.Errorf("could not obtain list of instances: %s", err)
	}

	for _, instance := range instanceArr.Items {

		if d.instanceMatchesFilterTags(instance) {

			vnicIpLabels, err := d.getVnicDetails(context, *instance.Id, mapSubnetIdToDetails)

			if err != nil {
				level.Error(d.logger).Log("msg", "Instance skipped from OCI SD because of error", "ocid", *instance.Id)
				continue
			}

			labels := model.LabelSet{
				ociLabelInstanceID:           model.LabelValue(*instance.Id),
				ociLabelInstanceName:         model.LabelValue(*instance.DisplayName),
				ociLabelInstanceState:        model.LabelValue(instance.LifecycleState),
				ociLabelCompartmentID:        model.LabelValue(*instance.CompartmentId),
				ociLabelFullCompartmentName:  model.LabelValue(tenancyCmptOcidToNameMap[*instance.CompartmentId]),
				ociLabelAvailabilityDomain:   model.LabelValue(*instance.AvailabilityDomain),
				ociLabelFaultDomain:          model.LabelValue(*instance.FaultDomain),
				ociLabelRegion:               model.LabelValue(*instance.Region),
				ociLabelOcpus:                model.LabelValue(fmt.Sprintf("%f", *instance.ShapeConfig.Ocpus)),
				ociLabelProcessorDescription: model.LabelValue(*instance.ShapeConfig.ProcessorDescription),
			}

			for key, value := range instance.FreeformTags {
				labels[ociLabelFreeformTag+model.LabelName(key)] = model.LabelValue(value)
			}
			for ns, tags := range instance.DefinedTags {
				for key, value := range tags {
					labelName := model.LabelName(ociLabelDefinedTag + ns + "_" + key)
					labels[labelName] = model.LabelValue(value.(string))
				}
			}

			labels = labels.Merge(*vnicIpLabels)

			tg.Targets = append(tg.Targets, labels)
		}

	}

	tgArr := []*targetgroup.Group{tg}
	return tgArr, nil

}

func (d *Discovery) getCompartments(ctx context.Context) (map[string]string, error) {
	m := make(map[string]string)

	tenancyOcid := d.cfg.Tenancy
	region := d.cfg.Region

	req := identity.GetTenancyRequest{TenancyId: common.String(tenancyOcid)}
	// Send the request using the service client
	resp, err := d.iamClient.GetTenancy(context.Background(), req)
	if err != nil {
		return nil, errors.Wrap(err, fmt.Sprintf("This is what we were trying to get %s", " : fetching tenancy name"))
	}

	mapFromIdToName := make(map[string]string)
	mapFromIdToName[tenancyOcid] = *resp.Name //tenancy name

	mapFromIdToParentCmptId := make(map[string]string)
	mapFromIdToParentCmptId[tenancyOcid] = "" //since root cmpt does not have a parent

	var page *string
	reg := common.StringToRegion(region)
	d.iamClient.SetRegion(string(reg))
	for {
		res, err := d.iamClient.ListCompartments(ctx,
			identity.ListCompartmentsRequest{
				CompartmentId:          &tenancyOcid,
				Page:                   page,
				AccessLevel:            identity.ListCompartmentsAccessLevelAny,
				CompartmentIdInSubtree: common.Bool(true),
			})
		if err != nil {
			return nil, errors.Wrap(err, fmt.Sprintf("this is what we were trying to get %s", tenancyOcid))
		}
		for _, compartment := range res.Items {
			if compartment.LifecycleState == identity.CompartmentLifecycleStateActive {
				mapFromIdToName[*(compartment.Id)] = *(compartment.Name)
				mapFromIdToParentCmptId[*(compartment.Id)] = *(compartment.CompartmentId)
			}
		}
		if res.OpcNextPage == nil {
			break
		}
		page = res.OpcNextPage
	}

	mapFromIdToFullCmptName := make(map[string]string)
	mapFromIdToFullCmptName[tenancyOcid] = mapFromIdToName[tenancyOcid] + "(Tenancy)"

	for len(mapFromIdToFullCmptName) < len(mapFromIdToName) {
		for cmptId, cmptParentCmptId := range mapFromIdToParentCmptId {
			_, isCmptNameResolvedFullyAlready := mapFromIdToFullCmptName[cmptId]
			if !isCmptNameResolvedFullyAlready {
				if cmptParentCmptId == tenancyOcid {
					// If tenancy/rootCmpt my parent
					// cmpt name itself is fully qualified, just prepend '/' for tenancy aka rootCmpt
					mapFromIdToFullCmptName[cmptId] = "Tenancy/" + mapFromIdToName[cmptId]
				} else {
					fullNameOfParentCmpt, isMyParentNameResolvedFully := mapFromIdToFullCmptName[cmptParentCmptId]
					if isMyParentNameResolvedFully {
						mapFromIdToFullCmptName[cmptId] = fullNameOfParentCmpt + "/" + mapFromIdToName[cmptId]
					}
				}
			}
		}
	}

	for cmptId, fullyQualifiedCmptName := range mapFromIdToFullCmptName {
		m[fullyQualifiedCmptName] = cmptId
	}

	return m, nil
}

func (d *Discovery) instanceMatchesFilterTags(instance core.Instance) bool {
	totalMatchCount := 0

	for _, tagFilter := range d.cfg.TagFilters {
		if tagFilter.TagNameSpace != "" {
			for ns, tags := range instance.DefinedTags {
				if tagFilter.TagNameSpace == ns {
					for key, value := range tags {
						if tagFilter.TagKey == key && tagFilter.TagValue == value {
							totalMatchCount++
							break
						}
					}
				}
			}
		} else {
			for key, value := range instance.FreeformTags {
				if tagFilter.TagKey == key && tagFilter.TagValue == value {
					totalMatchCount++
					break
				}
			}
		}
	}
	return totalMatchCount == len(d.cfg.TagFilters)
}

func (d *Discovery) getVnicDetails(ctx context.Context, instanceId string, mapSubnetIdToDetails map[string]SubnetInfo) (*model.LabelSet, error) {

	vnicAttchmentArr, err := d.computeClient.ListVnicAttachments(
		ctx,
		core.ListVnicAttachmentsRequest{
			CompartmentId: &d.cfg.Compartment,
			InstanceId:    &instanceId,
		},
	)
	if err != nil {
		level.Error(d.logger).Log("msg", "could not obtain attached vnics for instance", "ocid", instanceId)
		return nil, err
	}

	labels := model.LabelSet{}

	for _, vnic := range vnicAttchmentArr.Items {
		vnicDetails, err := d.vnwClient.GetVnic(ctx,
			core.GetVnicRequest{VnicId: vnic.VnicId},
		)
		if err != nil {
			level.Error(d.logger).Log("msg", "could not obtain vnic", "ocid", vnic.VnicId)
			return nil, err
		}
		if *vnicDetails.IsPrimary {
			labels[ociLabelPrivateIP] = model.LabelValue(*vnicDetails.PrivateIp)
			if *vnicDetails.PublicIp != "" {
				labels[ociLabelPublicIP] = model.LabelValue(*vnicDetails.PublicIp)
			}
			addr := net.JoinHostPort(*vnicDetails.PrivateIp, fmt.Sprintf("%d", d.cfg.Port))
			labels[model.AddressLabel] = model.LabelValue(addr)
		}

		labels[ociLabelSubnetId] = model.LabelValue(*vnicDetails.SubnetId)

		subnetVcnInfo, subnetinfoPresent := mapSubnetIdToDetails[*vnicDetails.SubnetId]
		if subnetinfoPresent {
			labels[ociLabelVcnId] = model.LabelValue(subnetVcnInfo.vcnOcid)
			labels[ociLabelVcn] = model.LabelValue(subnetVcnInfo.vcnName)
		}

	}

	return &labels, nil
}

type SubnetInfo struct {
	vcnOcid string
	vcnName string
}

func (d *Discovery) getVcnDetails(ctx context.Context, vnwClient *core.VirtualNetworkClient, cmptId string) (map[string]SubnetInfo, error) {

	vcnList, err := vnwClient.ListVcns(ctx, core.ListVcnsRequest{CompartmentId: &cmptId})
	if err != nil {
		level.Error(d.logger).Log("msg", "could not obtain vcn list for compartmentId", "ocid", cmptId)
		return nil, err
	}

	mapVcnIdToName := make(map[string]string)
	for _, vcn := range vcnList.Items {
		mapVcnIdToName[*vcn.Id] = *vcn.DisplayName
	}

	subnetList, err := vnwClient.ListSubnets(ctx, core.ListSubnetsRequest{CompartmentId: &cmptId})
	if err != nil {
		level.Error(d.logger).Log("msg", "could not obtain subnet list for compartmentId", "ocid", cmptId)
		return nil, err
	}
	mapSubNetDetails := make(map[string]SubnetInfo)
	for _, sbnet := range subnetList.Items {
		mapSubNetDetails[*sbnet.Id] = SubnetInfo{*sbnet.VcnId, mapVcnIdToName[*sbnet.VcnId]}
	}
	return mapSubNetDetails, nil

}
